package com.ub.start.blog.controller;


import com.ub.core.base.utils.RouteUtils;
import com.ub.core.base.view.PageHeader;
import com.ub.core.base.view.search.SearchRequest;
import com.ub.core.picture.model.PictureDoc;
import com.ub.core.picture.repository.PictureRepository;
import com.ub.start.blog.model.BlogDoc;
import com.ub.start.blog.repository.BlogRepository;
import com.ub.start.blog.route.BlogAdminRoute;
import com.ub.start.blog.view.search.SearchBlogRequest;
import com.ub.start.blog.view.search.SearchBlogResponse;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller
public class BlogAdminController {
    @Autowired private BlogRepository blogRepository;
    @Autowired private PictureRepository pictureRepository;

    @RequestMapping(value = BlogAdminRoute.ADD, method = RequestMethod.GET)
    public String add(Model model){

        BlogDoc blogDoc = new BlogDoc();

        blogDoc.setId(new ObjectId());
        blogDoc.setPicId(new ObjectId());

        model.addAttribute("doc", blogDoc);
        return "com.ub.start.blog.admin.add";
    }

    @RequestMapping(value = BlogAdminRoute.ADD, method = RequestMethod.POST)
    public String add(@ModelAttribute BlogDoc blogDoc,
                      @RequestParam(required = false) MultipartFile pic, RedirectAttributes ra){

        PictureDoc pictureDoc = pictureRepository.save(blogDoc.getPicId() , pic);
        blogDoc.setPicId(pictureDoc.getId());
        blogDoc = blogRepository.save(blogDoc);
        ra.addAttribute("id", blogDoc.getId().toString());
        return RouteUtils.redirectTo(BlogAdminRoute.EDIT);
    }


    @RequestMapping(value = BlogAdminRoute.EDIT, method = RequestMethod.GET)
    public String edit(@RequestParam ObjectId id, Model model){

        BlogDoc blogDoc = blogRepository.findById(id);
        model.addAttribute("doc", blogDoc);

        return "com.ub.start.blog.admin.edit";

    }

    @RequestMapping(value = BlogAdminRoute.EDIT, method = RequestMethod.POST)
    public String edit(@ModelAttribute BlogDoc blogDoc,
                       @RequestParam MultipartFile pic,RedirectAttributes ra ){


        PictureDoc pictureDoc = pictureRepository.save(blogDoc.getPicId() , pic);
        blogDoc.setPicId(pictureDoc.getId());
        blogDoc = blogRepository.save(blogDoc);
        ra.addAttribute("id", blogDoc.getId());

        return RouteUtils.redirectTo(BlogAdminRoute.EDIT);

    }

    @RequestMapping(value = BlogAdminRoute.ALL, method = RequestMethod.GET)
    public String all(@ModelAttribute SearchBlogRequest request, Model model){

        model.addAttribute("search", blogRepository.findAll(request));
        return "com.ub.start.blog.admin.all";
    }

    @RequestMapping(value = BlogAdminRoute.REMOVE, method = RequestMethod.GET)
    public String remove(@RequestParam ObjectId id, Model model){
        model.addAttribute("id", id);
        return "com.ub.start.blog.admin.remove";
    }

    @RequestMapping(value = BlogAdminRoute.REMOVE, method = RequestMethod.POST)
    public String remove(@RequestParam ObjectId id){
        blogRepository.remove(id);
        return RouteUtils.redirectTo(BlogAdminRoute.ALL);
    }
}
