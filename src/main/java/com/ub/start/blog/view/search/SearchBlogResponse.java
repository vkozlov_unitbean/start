package com.ub.start.blog.view.search;

import com.ub.core.base.view.search.SearchResponse;
import com.ub.start.blog.model.BlogDoc;

import java.util.List;

public class SearchBlogResponse extends SearchResponse{
    private List<BlogDoc> result;

    public SearchBlogResponse(){

    }
    public SearchBlogResponse(Integer currentPage, Integer pageSize, List<BlogDoc> result){
        this.pageSize = pageSize;
        this.currentPage = currentPage;
        this.result = result;
    }

    public List<BlogDoc> getResult() {
        return result;
    }

    public void setResult(List<BlogDoc> result) {
        this.result = result;
    }
}
