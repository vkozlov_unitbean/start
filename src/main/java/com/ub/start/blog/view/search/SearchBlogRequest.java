package com.ub.start.blog.view.search;

import com.ub.core.base.view.search.SearchRequest;

public class SearchBlogRequest extends SearchRequest{
    public SearchBlogRequest(){}
    public SearchBlogRequest(Integer currentPage){
        this.currentPage = currentPage;
    }
    public SearchBlogRequest(Integer currentPage,Integer pageSize){
        this.pageSize = pageSize;
        this.currentPage = currentPage;
    }
}
