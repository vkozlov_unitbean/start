package com.ub.start.blog.menu;

import com.ub.core.base.menu.BaseMenu;
import com.ub.core.base.menu.MenuIcons;
import com.ub.start.blog.route.BlogAdminRoute;

public class BlogAllMenu extends BaseMenu{
    public BlogAllMenu(){
        this.name = "Все";
        this.icon = MenuIcons.MDI_ACTION_DESCRIPTION;
        this.parent = new BlogMenu();
        this.url = BlogAdminRoute.ALL;
    }


}
