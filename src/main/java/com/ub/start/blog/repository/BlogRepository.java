package com.ub.start.blog.repository;


import com.ub.start.blog.model.BlogDoc;
import com.ub.start.blog.view.search.SearchBlogRequest;
import com.ub.start.blog.view.search.SearchBlogResponse;
import javafx.print.Collation;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.List;

@Component
public class BlogRepository {

    @Autowired private MongoTemplate mongoTemplate;

    public BlogDoc save(BlogDoc blogDoc){
        mongoTemplate.save(blogDoc);
        return blogDoc;
    }

    public BlogDoc findById(ObjectId id) {
        return mongoTemplate.findById(id, BlogDoc.class);
    }

    public SearchBlogResponse findAll(SearchBlogRequest request){
        Sort sort = new Sort(Sort.Direction.DESC, "id");

        Criteria criteria = new Criteria();
        criteria.orOperator(
                Criteria.where("title").regex(request.getQuery(), "i"),
                Criteria.where("description").regex(request.getQuery(), "i")
        );



        Query query = new Query(criteria);
        Long count = mongoTemplate.count(query, BlogDoc.class);

        query.limit(request.getPageSize());
        query.skip(request.getCurrentPage() * request.getPageSize());
        query.with(sort);

        List<BlogDoc> blogs = mongoTemplate.find(query, BlogDoc.class);

        SearchBlogResponse searchBlogResponse = new SearchBlogResponse();
        searchBlogResponse.setAll(count);
        searchBlogResponse.setCurrentPage(request.getCurrentPage());
        searchBlogResponse.setPageSize(request.getPageSize());
        searchBlogResponse.setQuery(request.getQuery());
        searchBlogResponse.setResult(blogs);

        return searchBlogResponse;
    }

    public void remove(ObjectId id){
        BlogDoc blogDoc = this.findById(id);
        mongoTemplate.remove(blogDoc);
    }

    public List<BlogDoc> findAll(){
        Sort sort = new Sort(Sort.Direction.DESC, "id");
        List<BlogDoc> blogs = mongoTemplate.find(new Query().with(sort), BlogDoc.class);
        return blogs;
    }
}
