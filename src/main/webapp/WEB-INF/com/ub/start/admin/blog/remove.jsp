<%@ page import="com.ub.start.blog.route.BlogAdminRoute" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>


<form action="<%= BlogAdminRoute.REMOVE %>" method="post">
    <input type="hidden" name="id" value="${id}">
    <h1>Внимание!</h1>
    <p>Документ будет удален!</p>
    <button class="btn red">Удалить</button>
</form>