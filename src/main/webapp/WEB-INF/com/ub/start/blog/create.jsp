<%--
  Created by IntelliJ IDEA.
  User: vladimir
  Date: 20.07.17
  Time: 10:54
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>


<html>
<head>
    <title>Create</title>
    <link rel="stylesheet" href="static/start/css/style.css">
    <link href="https://fonts.googleapis.com/css?family=Ubuntu" rel="stylesheet">
</head>
<body>


<div class="content mod-create">
    <c:set value="${doc}" var="doc" scope="request"/>
    <div class="blog-create">
        <form:form action="/success" class="card-panel" modelAttribute="doc" method="post" enctype="multipart/form-data">
            <h1 class="blog-create-title">Добавление статьи</h1>

            <div class="blog-create-group">
                <lable class="blog-create-lable">Заголовок</lable>
                <form:input path="title" id="title" class="blog-create-input"/>
            </div>

            <div class="blog-create-group">
                <lable class="blog-create-lable">Описание</lable>
                <form:textarea class="blog-create-input mod-textarea" path="description" id="description"></form:textarea>
            </div>

            <div class="blog-create-footer">
                <lable class="blog-create-lable">Прикрепить изображение</lable>
                <div class="blog-create-file">
                    <input type="file" class="blog-create-file-input" name="pic">
                </div>
                <form:button class="btn btn-approve" type="submit" name="action">Добавить</form:button>
                <a href="/" class="btn btn-cancel">Отмена</a>
            </div>
        </form:form>
    </div>
</div>
</body>
</html>
