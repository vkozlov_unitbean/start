<%--
  Created by IntelliJ IDEA.
  User: vladimir
  Date: 24.07.17
  Time: 16:26
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<html>
<head>
    <title>Статья</title>
    <link rel="stylesheet" href="static/start/css/style.css">
    <link href="https://fonts.googleapis.com/css?family=Ubuntu" rel="stylesheet">
</head>
<body>
    <c:set value="${article}" var="doc" scope="request"/>
    <div class="head">
        <div class="article-title">
            <h1>${article.title}</h1>
        </div>
    </div>
    <div class="content">
        <div class="article-img" style="background-image: url('/pics/${article.picId}')">
        </div>
        <div class="article-date">
            <fmt:formatDate type="both" pattern="yyyy-MM"
                            value="${article.updateAt}"/>
        </div>
        <div class="article-text">
            ${article.description}
        </div>

    </div>
    <div class="article-footer">
        <a href="/" class="btn mod-article-btn-back">Назад</a>
    </div>
</body>
</html>
