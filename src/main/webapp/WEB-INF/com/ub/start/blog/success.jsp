<%--
  Created by IntelliJ IDEA.
  User: vladimir
  Date: 20.07.17
  Time: 10:54
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Success</title>
    <link rel="stylesheet" href="static/start/css/style.css">
    <link href="https://fonts.googleapis.com/css?family=Ubuntu" rel="stylesheet">
</head>
<body>


<div class="content">
    <div class="success">
        <img src="/static/start/img/logo-red.png" class="success-logo">
        <h1 class="success-title">Поздравляем Вас с успешным добавлением статьи в блог UnitBean!</h1>
    </div>
</div>
</body>
</html>
