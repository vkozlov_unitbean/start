<%@ page import="com.ub.start.blog.route.BlogRoute" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%--
  Created by IntelliJ IDEA.
  User: vladimir
  Date: 20.07.17
  Time: 10:54
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
    <link rel="stylesheet" href="static/start/css/style.css">
    <link href="https://fonts.googleapis.com/css?family=Ubuntu" rel="stylesheet">
</head>
<body>
<div class="head">
    <div class="head-logo"></div>
    <div class="head-count">
        Статьи, 24
    </div>
    <a href="/create" class="head-blog-create"></a>
</div>

<div class="content">
    <c:forEach items="${blogs}" var="blog" varStatus="status">
        <div class="blog-item">
            <div class="blog-item-background" style="background-image: url('/pics/${blog.picId}')"></div>
            <div class="blog-item-title">
                    ${blog.title}
            </div>
            <div class="blog-item-date">
                    <fmt:formatDate type="both" pattern="yyyy-MM"
                                    value="${blog.updateAt}"/>
            </div>
            <div class="blog-item-hr"></div>
            <div class="blog-item-text">
                    ${blog.description}
            </div>
            <c:url value="<%=BlogRoute.ARTICLE%>" var="editUrl">
                <c:param name="id" value="${blog.id}"/>
            </c:url>
            <a href="${editUrl}"  class="blog-item-more">Читать дальше...</a>
        </div>
    </c:forEach>


</div>
</body>
</html>
